//Hårte PWM Handling
//(c) Konrad Rosenbaum, 2022
//protected under GNU GPL v.3 or at your option any newer

#pragma once

void pwminit();
void pwmloop();

void pwmtoggle(uint8_t output);
void pwmincr(uint8_t output);
void pwmdecr(uint8_t output);

bool pwmismax(uint8_t output);
uint8_t pwmvalue(uint8_t output);
void pwmset(uint8_t output, bool setval);
