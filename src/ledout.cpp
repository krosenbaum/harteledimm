//Hårte LED Output Handling
//(c) Konrad Rosenbaum, 2022
//protected under GNU GPL v.3 or at your option any newer

#include "ledout.h"
#include "pindefs.h"

void ledoutinit()
{
    //configure status LED port
    LEDPORT.DIR      = 0xff; // all output
    LEDPORT.OUT      = 0; // reset output to LOW
    LEDPORT.PIN0CTRL = 0; // no inversion
    LEDPORT.PIN1CTRL = 0;
    LEDPORT.PIN2CTRL = 0;
    LEDPORT.PIN3CTRL = 0;
    LEDPORT.PIN4CTRL = 0;
    LEDPORT.PIN5CTRL = 0;
    LEDPORT.PIN6CTRL = 0;
    LEDPORT.PIN7CTRL = 0;
}

void ledoutloop()
{}
