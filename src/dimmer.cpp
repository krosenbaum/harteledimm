//Hårte Main Program
//(c) Konrad Rosenbaum, 2022
//protected under GNU GPL v.3 or at your option any newer


#include "pindefs.h"

#include <util/delay.h>

#include "dmx.h"
#include "input.h"
#include "pwm.h"
#include "ledout.h"

///initialize pins and basic settings
void initchip()
{
    //configure clock source
    CLKCTRL.MCLKCTRLA = MCLKCTRLA_VAL;
    CLKCTRL.MCLKCTRLB = MCLKCTRLB_VAL;

    //init other modules
    inputinit();
//     ledoutinit();
    pwminit();
//     dmxinit();
}

int main(void)
{
    initchip();

    for(;;){
        inputloop();
        pwmloop();
//         ledoutloop();
//         dmxloop();
        _delay_us(1);
    }

    return 0;
}
