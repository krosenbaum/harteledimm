//Hårte DMX receiver
//(c) Konrad Rosenbaum, 2022
//protected under GNU GPL v.3 or at your option any newer

#include "dmx.h"
#include "pindefs.h"

//baud rate
#define DMXBAUD 250000L
//interrupt enable
#define DMXRECVINT 0 //TODO: create interrupt handler
#define DMXSENDINT 0
//overall recv/send enable
#define DMXRECVEN 1
#define DMXSENDEN 0

void dmxinit()
{
    //configure MUX for pin routing
    UARTMUXREG = UARTMUXVAL; //UART

    //configure UART for DMX (250 kBaud, 8N2)
    DMXUART.BAUD = (long)F_CPU * 64L / DMXBAUD / 16 ;
    DMXUART.CTRLA =
        (DMXRECVINT ? 0x80 : 0) | // 7: receive interrupt
        (DMXSENDINT ? 0x40 : 0) | // 6: send interrupt
        0 | // 5: data register empty interrupt
        0 | // 4: receiver start frame interrupt
        0 | // 3: no loopback
        0 | // 2: no auto baud rat error interrupt
        1 ; // 0:1 RS-485 mode
    DMXUART.CTRLB =
        (DMXRECVEN ? 0x80 : 0) | // 7: receive enable
        (DMXSENDEN ? 0x40 : 0) | // 6: transmit enable
        //5: unused
        0 | // 4: start of frame detection
        0 | // 3: open drain mode
        0x00 | // 2,1: Speed: 00=normal, 01=double, 10=auto baud, 11=LIN constrained auto baud
        0 ; // Multi-Processor Communication Mode
    DMXUART.CTRLC =
        0 | // 7,6: comm mode 00=async, 01=sync, 10=ircom, 11=mspi
        0 | // 5,4: parity 00=none, 10=even, 11=odd
        (1<<3) | // 3: stop bits 0=1bit 1=2bit
        0x03 ; // 2-0: char size 0=5bit 1=6bit 2=7bit 3=8bit 6/7=9bit l/h
    DMXUART.CTRLD = 0; // auto baud window
}

void dmxloop()
{
}
