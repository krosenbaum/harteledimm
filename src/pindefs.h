//Hårte Pin Definitions
//(c) Konrad Rosenbaum, 2022
//protected under GNU GPL v.3 or at your option any newer

#pragma once

#include <avr/io.h>

//hack to convince KDevelop to load the correct MCU header
#ifndef _AVR_IOXXX_H_
#warning "Ooops. This should not leak beyond KDev."
#include <avr/iom3208.h>
#endif

/* Pins   Function       Single Multi
 * 22 A0: Detect1        GND    Plug1
 * 23 A1: Detect2        -      Plug2
 * 24 A2: Detect3        -      Plug3
 * 25 A3: Detect4        -      Plug4
 * 26 A4: RE_L           Rotary-Encoder
 * 27 A5: RE_R           Rotary-Encoder
 * 28 A6: RE_B           Rotary-Encoder
 *  1 A7: IN_Xtra        -      Button
 *
 *  - B*: not exist
 *
 *  2 C0: PWM1           Output Plug1
 *  3 C1: PWM2           -      Plug2
 *  4 C2: PWM3           -      Plug3
 *  5 C3: PWM4           -      Plug4
 *  - C*: not exist
 *
 *  6 D0: LED 1          -      LED
 *  7 D1: LED 2          -      LED
 *  8 D2: LED 3          -      LED
 *  9 D3: LED 4          -      LED
 * 10 D4: LED A          -      LED
 * 11 D5: LED B          -      LED
 * 12 D6: LED C          -      LED
 * 13 D7: LED D          -      LED
 *
 * 14 AVdd
 * 15 GND
 *
 *  - E*: not exist
 *
 * 16 F0: USART2 Tx      -      DMX
 *       -> DMX out
 * 17 F1: USART2 Rx      -      DMX
 *       -> DMX in
 *  - F*: not exist
 * 18 F6: DMX direction  -      DMX
 *       (1=Tx, 0=Rx)
 *
 * 19 UPDI               UPDI-Connector
 * 20 Vdd                UPDI-Connector/Input
 * 21 GND                UPDI-Connector/Input
 */

//Clock Config
//F_CPU is defined in Makefile, next to clock fuse settings
#define MCLKCTRLA_VAL 0x0 //no clockout (0x80), use internal 16/20MHz (0x0-0x3)
#define MCLKCTRLB_VAL 0x0 //no prescaler (PEN=0), (DIV=0x0)

//Input Port
#define INPUTPORT PORTA
//Detector Pins
#define DETECT1 PIN0_bm
#define DETECT2 PIN1_bm
#define DETECT3 PIN2_bm
#define DETECT4 PIN3_bm
//Rotary Encoder Pins
#define RE_L PIN4_bm
#define RE_R PIN5_bm
#define RE_B PIN6_bm
//Extra Button
#define IN_Xtra PIN7_bm

//LED Pins
#define LEDPORT PORTD
#define LED_1 PIN0_bm
#define LED_2 PIN1_bm
#define LED_3 PIN2_bm
#define LED_4 PIN3_bm
#define LED_A PIN4_bm
#define LED_B PIN5_bm
#define LED_C PIN6_bm
#define LED_D PIN7_bm
//TODO: port config

//PWM Config (using TCA)
#define PWM_CHANNELS 4
#define PWMPORT PORTC
#define PWM_PORT_DIR ((1<<PWM_CHANNELS)-1)

#define PWM_CMPEN TCA_SPLIT_LCMP0EN_bm | TCA_SPLIT_LCMP1EN_bm | TCA_SPLIT_LCMP2EN_bm |TCA_SPLIT_HCMP0EN_bm

#define PWM_CMP0 TCA0.SPLIT.LCMP0
#define PWM_CMP1 TCA0.SPLIT.LCMP1
#define PWM_CMP2 TCA0.SPLIT.LCMP2
#define PWM_CMP3 TCA0.SPLIT.HCMP0

////
//Port MUX Config

//PWM routing
#define TCMUXVAL PORTMUX_TCA0_PORTC_gc

//UART MUX helpers
#define UART_DEFAULT_PINS 0
#define UART_ALT1_PINS 1
#define UART_NO_PINS 3
#define UART0_MUX 0
#define UART1_MUX 2
#define UART2_MUX 4
#define UART3_MUX 6

//UART routing
#define UARTMUXREG PORTMUX_USARTROUTEA
#define UARTMUXVAL \
    UART_NO_PINS        << UART0_MUX | \
    UART_NO_PINS        << UART1_MUX | \
    UART_DEFAULT_PINS   << UART2_MUX | \
    UART_NO_PINS        << UART3_MUX

//UART config
#define DMXUART USART2
