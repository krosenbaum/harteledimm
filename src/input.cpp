//Hårte Input Handling
//(c) Konrad Rosenbaum, 2022
//protected under GNU GPL v.3 or at your option any newer

#include "pindefs.h"
#include "input.h"
#include "pwm.h"

void inputinit()
{
    //configure input port
    INPUTPORT.DIR      = 0; //all input
    INPUTPORT.PORTCTRL = 1 << PORT_SRL_bp; //slew rate limiting (buttons are not fast, but sometimes jitter)
    const uint8_t ctrl = 1 << PORT_PULLUPEN_bp | 1 << PORT_INVEN_bp; //pull-up R, inverted value, no interrupt
    INPUTPORT.PIN0CTRL = ctrl;
    INPUTPORT.PIN1CTRL = ctrl;
    INPUTPORT.PIN2CTRL = ctrl;
    INPUTPORT.PIN3CTRL = ctrl;
    INPUTPORT.PIN4CTRL = ctrl;
    INPUTPORT.PIN5CTRL = ctrl;
    INPUTPORT.PIN6CTRL = ctrl;
    INPUTPORT.PIN7CTRL = ctrl;
}

#define RE_DIR (RE_L|RE_R)

void inputloop()
{
    static uint8_t old_in=0;
    const uint8_t in=INPUTPORT.IN;
    if(old_in==in)return;
    //rot.enc. button check
    if((old_in&RE_B) != (in&RE_B))
        if(in&RE_B)pwmtoggle(0);
    //rot.enc. directions (to 0-1-3-2-0; fro 0-2-3-1-0)
    if((old_in&RE_DIR) != (in&RE_DIR)){
        if((old_in&RE_DIR)==RE_L && (in&RE_DIR)==0)pwmincr(0);
        if((old_in&RE_DIR)==RE_R && (in&RE_DIR)==0)pwmdecr(0);
    }
    //TODO: xtra button
    //done
    old_in=in;
}
