//Hårte DMX receiver
//(c) Konrad Rosenbaum, 2022
//protected under GNU GPL v.3 or at your option any newer

#pragma once

void dmxinit();
void dmxloop();
