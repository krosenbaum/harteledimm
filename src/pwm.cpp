//Hårte PWM Handling
//(c) Konrad Rosenbaum, 2022
//protected under GNU GPL v.3 or at your option any newer

#include "pindefs.h"

#include "pwm.h"

#include <string.h>

#define PWM_MAX 64
#define PWM_DEFAULT (PWM_MAX/4)

static uint8_t pwm_setval[4];
static uint8_t pwm_curval[4];

void pwminit()
{
    //send TCA to the right output
    PORTMUX.TCAROUTEA = TCMUXVAL; //PWM

    //set port to output
    PWMPORT.DIR = PWM_PORT_DIR;

    //enable split mode
    TCA0.SPLIT.CTRLD=1;
//     1. Write a TOP value to the Period (TCAn.PER) register.
    TCA0.SPLIT.HPER=PWM_MAX;
    TCA0.SPLIT.LPER=PWM_MAX;
//     2. Enable the peripheral by writing a ‘1’ to the ENABLE bit in the Control A (TCAn.CTRLA) register.
//     The counter will start counting clock ticks according to the prescaler setting in the Clock Select (CLKSEL) bit
//     field in the TCAn.CTRLA register.
    TCA0.SPLIT.CTRLA = TCA_SPLIT_CLKSEL_DIV64_gc | 1; //enabled, max speed
//     3. Optional: By writing a ‘1’ to the Enable Count on Event Input (CNTEI) bit in the Event Control (TCAn.EVCTRL)
//     register, events are counted instead of clock ticks.
    //leave at 0
//     4. The counter value can be read from the Counter (CNT) bit field in the Counter (TCAn.CNT) register.
    TCA0.SPLIT.CTRLB=PWM_CMPEN;//enable compares

    //init set values
    memset(pwm_setval,PWM_DEFAULT,sizeof(pwm_setval));
    memset(pwm_curval,0,sizeof(pwm_curval));
}

void pwmloop()
{
    if(pwm_curval[0] != pwm_setval[0]){ PWM_CMP0 = pwm_setval[0]; pwm_curval[0] = pwm_setval[0]; }
    if(pwm_curval[1] != pwm_setval[1]){ PWM_CMP1 = pwm_setval[1]; pwm_curval[1] = pwm_setval[1]; }
    if(pwm_curval[2] != pwm_setval[2]){ PWM_CMP2 = pwm_setval[2]; pwm_curval[2] = pwm_setval[2]; }
    if(pwm_curval[3] != pwm_setval[3]){ PWM_CMP3 = pwm_setval[3]; pwm_curval[3] = pwm_setval[3]; }
}

void pwmtoggle(uint8_t output)
{
    if(output<0 || output>=PWM_CHANNELS)return;
    if(pwm_setval[output]==0)pwm_setval[output]=PWM_MAX;
    else pwm_setval[output]=0;
}
void pwmincr(uint8_t output)
{
    if(output<0 || output>=PWM_CHANNELS)return;
    if(pwm_setval[output]<PWM_MAX)pwm_setval[output]++;

}
void pwmdecr(uint8_t output)
{
    if(output<0 || output>=PWM_CHANNELS)return;
    if(pwm_setval[output]>0)pwm_setval[output]--;
}

bool pwmismax(uint8_t output)
{
    if(output<0 || output>=PWM_CHANNELS)return false;
    return pwm_setval[output]<PWM_MAX;
}

uint8_t pwmvalue(uint8_t output)
{
    if(output<0 || output>=PWM_CHANNELS)return 0;
    return pwm_setval[output];
}

void pwmset(uint8_t output, bool setval)
{
    if(output<0 || output>=PWM_CHANNELS)return;
    pwm_setval[output] = setval ? PWM_MAX : 0;
}
